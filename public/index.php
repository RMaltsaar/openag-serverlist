<?php

require_once "../vendor/autoload.php";

use peterkahl\flagMaster\flagMaster;

require_once "../config.php";

$globeEmojiArray = [
    "🌎",
    "🌍",
    "🌏",
];

$servers = [];
$serversLastUpdated = [];

try {
    $db = new SQLite3("../database/" . $database, SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);

    $serversResult = $db->query("SELECT * FROM 'servers'");
    $serversResultData = $serversResult->fetchArray(1);
    while ($serversResultData) {
        $serversResultData['emoji'] = flagMaster::emojiFlag($serversResultData["countryCode"]);
        $servers[] = $serversResultData;
        $serversResultData = $serversResult->fetchArray(1);
    }
    $serversLastUpdatedResult = $db->query("SELECT * FROM 'last-updated'");
    $serversLastUpdatedResultData = $serversLastUpdatedResult->fetchArray(1);
    while ($serversLastUpdatedResultData) {
        $serversLastUpdated[] = $serversLastUpdatedResultData;
        $serversLastUpdatedResultData = $serversLastUpdatedResult->fetchArray(1);
    }
    if (empty($serversLastUpdated) || empty($servers)) {
        $errorType = "dbEmpty";
    }
} catch (Exception $e) {
    $errorType = "db";
}

require_once("../templates/htmlHeader.php");

if (!empty($errorType)) {

    switch ($errorType) {
        case "db":
            $errorText = "There's an issue with reading database... Run setupDatabase.php?";
            break;
        case "dbEmpty":
            $errorText = "The database is empty... Run updateDatabase.php?";
            break;
        default:
            $errorText = "Something happened!";
            break;
    }
    ?>
        <div class="columns">
            <div class="column">
                <div class="box<?= (empty($servers)) ? " marginDown" : '' ?>">
                    <span><strong>Error: </strong><?= $errorText ?> 💢</span>
                </div>
            </div>
        </div>
        <?php

    }

    if (!empty($servers)) { ?>
    		<div class="servers">
                <table class="table is-boxed is-hoverable is-bordered is-fullwidth" id="table">
                    <thead>
                        <tr>
                            <th class="sortable">#</th>
                            <th class="sortable">IP</td>
                            <th class="sortable">Hostname</td>
                            <th class="sortable">Game</td>
                            <th class="sortable">Min</td>
                            <th class="sortable">Max</td>
                            <th class="sortable">Map</td>
                            <th class="sortable"><?= $globeEmojiArray[array_rand($globeEmojiArray)] ?></td>
                            <th data-sort-method="none">Connect?</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($servers as $server) { ?>
                            <tr>
                                <th><span><?= $server["nr"] ?></span></td>
                                <td><span><?= $server["ip"] ?></span></td>
                                <td><span><?= $server["hostname"] ?> <a class="modal-button is-info is-pulled-right" data-target="modal-<?= $server["nr"] ?>"><span class="icon"><i class="fas fa-info-circle"></span></i></a></span></td>
                                <td><span><?= $server["modDesc"] ?></span></td>
                                <td><span><?= $server["players"] ?></span></td>
                                <td><span><?= $server["maxPlayers"] ?></span></td>
                                <td><span><?= $server["map"] ?></span></td>
                                <td><span title="<?= $server["countryCode"] ?>"><?= $server["emoji"] ?></span></td>
                                <td><a href="steam://connect/<?= $server["ip"] ?>" class="button is-info is-small"><b>Connect</b></a></td>
                            </tr>
                        <?php 
                    } ?>
                    </tbody>
                </table>
            </div>
    <?php foreach ($servers as $server) { $playerInfo = json_decode($server["playerInfo"], true); $serverRules = json_decode($server["serverRules"], true); ?>

        <div class="modal" id="modal-<?= $server["nr"] ?>">
  <div class="modal-background animated fadeIn"></div>
  <div class="modal-card animated zoomIn">
  	<section class="modal-card-header">
  		<?php
  			if (file_exists("images/maps/".$server["map"].".png")) {
  				$mapFile = $server["map"].".png";
  			} else {
  				$mapFile = "missing-image";
  			}
  		?>
  		<div class="modalBanner hero is-info is-primary" style="background-image:url('/images/maps/<?= $mapFile ?>');">
			<h1 class="modalTitleHeader"><?= $server["hostname"] ?></h1>
			<h1 class="modalSubtitleHeader"><?= $server["players"]."/".$server["maxPlayers"] ?></h1>
  		</div>
  	</section>
    <section class="modal-card-body">
				<h1 class="modalTitleBody">General info</h1>
				<table class="table modalTable is-boxed is-hoverable is-bordered">
					<tbody>
						<tr class="hoverColor">
							<th>#</th>
							<td><?= $server["nr"] ?></td>
						</tr>
						<tr class="hoverColor">
							<th>IP</th>
							<td><?= $server["ip"] ?>
						</tr>
						<tr class="hoverColor">
							<th>Hostname</th>
							<td><?= $server["hostname"] ?></td>
						</tr>
						<tr class="hoverColor">
							<th>Game</th>
							<td><?= $server["modDesc"] ?></td>
						</tr>
						<tr class="hoverColor">
							<th>Players</th>
							<td><?= $server["players"] ?></td>
						</tr>
						<tr class="hoverColor">
							<th>Maximum players</th>
							<td><?= $server["maxPlayers"] ?></td>
						</tr>
						<tr class="hoverColor">
							<th>Map</th>
							<td><?= $server["map"] ?></td>
						</tr>
						<tr class="hoverColor">
							<th>Country</th>
							<td><span><?= $server["countryCode"] ?> </span><span><?= $server["emoji"] ?></span></td>
						</tr>
						<tr class="hoverColor">
							<th>Connect?</th>
							<td><a href="steam://connect/<?= $server["ip"] ?>" class="button is-info is-small"><b>Connect</b></a></td>
						</tr>
					</tbody>
				</table>
				<?php if (!empty($playerInfo)) { ?>
				<h1 class="modalTitleBody">Player info</h1>
			    <table class="table modalTable is-boxed is-hoverable is-bordered">
			        <thead>
			            <tr>
			                <th class="sortable">Players</th>
			                <th class="sortable">Kills</th>
			                <th class="sortable">Time online</th>
			            </tr>
			        </thead>
					<tbody>
			    <?php foreach ($playerInfo as $individualPlayerInfo) { ?>
			    		<tr>
			    			<td><?= color_player_name($individualPlayerInfo["Name"]) ?></td>
			    			<td><?= $individualPlayerInfo["Frags"] ?></td>
			    			<td><?= $individualPlayerInfo["TimeF"] ?></td>
			    		</tr>
			    <?php } ?>
			    	</tbody>
			    </table>
			<?php } if(!empty($serverRules)) { ?>
				<h1 class="modalTitleBody">Server rules</h1>
				<table class="table modalTable modalTableLast is-boxed is-hoverable is-bordered">
					<thead>
						<tr>
							<th class="sortable">Command</th>
							<th class="sortable">Value</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($serverRules as $key=>$individualServerRule) {
							if (!empty($individualServerRule)) {
								echo "<tr><td>$key</td><td>$individualServerRule</td></tr>";
							}}} 
						?>	
					</tbody>
				</table>
    </section>
  </div>
</div>
    <?php 
    }
}

require_once("../templates/htmlFooter.php");

function color_player_name($name)
{
	$len = strlen($name);
	$new_name = "";
	$font_open = false;
	$state = 0;
	for ($i = 0; $i < $len; ++$i) {
		$c = $name[$i];

		switch ($state) {
			case 0:
				if ($c == '^') {
					$state = 1;
				} else {
					$new_name .= $c;
				}
				break;

			case 1:
				if ($c == '0' || $c == '9') {
					if ($font_open) {
						$new_name .= "</font>";
						$font_open = false;
					}
				} else if ($c >= '1' && $c <= '8') {
					$color = "";
					switch ($c) {
						case '1':
							$color = "FF0000";
							break;
						case '2':
							$color = "00FF00";
							break;
						case '3':
							$color = "FFFF00";
							break;
						case '4':
							$color = "0000FF";
							break;
						case '5':
							$color = "00FFFF";
							break;
						case '6':
							$color = "FF00FF";
							break;
						case '7':
							$color = "888888";
							break;
						case '8':
							$color = "#808080
							";
							break;
					}

					if ($font_open) {
						$new_name .= "</font>";
					} else {
						$font_open = true;
					}

					$new_name .= "<font color=\"$color\">";
				} else {
					$new_name .= "^$c";
				}

				$state = 0;
				break;
		}
	}

	if ($state == 1) {
		$new_name .= "^";
	}
	if ($font_open) {
		$new_name .= "</font>";
	}

	return $new_name;
}