<?php

require_once "../vendor/autoload.php";
use xPaw\SourceQuery\SourceQuery;

$database = "servers.sqlite3"; // name of the database file in the database/ directory
$masterServer = "hl2master.steampowered.com:27011"; // master server to query
$blacklist = ["85.14.201.224:27015"]; // ip blacklist of servers that the updateDatabase.php should not include in the db

define('SQ_ENGINE', SourceQuery::GOLDSOURCE); // engine that the game uses (either GOLDSOURCE or SOURCE)

$appid = "70"; // appid of the game you're querying
$gamedir = "ag"; // if you don't want to query a specific mod leave it empty

// don't touch anything below this point.

if (empty($gamedir)) {
	$gamedirLine = "";
} else {
	$gamedirLine = "\\gamedir\\$gamedir";
}

$queryFilter = "appid\\$appid$gamedirLine";