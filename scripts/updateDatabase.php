<?php

error_reporting(E_ERROR | E_PARSE); // This is only here to disable the PHP Notice messages steam condenser sends

require_once "../vendor/autoload.php";
require_once "../vendor/koraktor/steam-condenser/lib/steam-condenser.php";
require_once "../config.php";

use xPaw\SourceQuery\SourceQuery;
use GeoIp2\Database\Reader;

$reader = new Reader('../resources/GeoLite2-Country.mmdb');

echo "Using: \"$queryFilter\" as master server query filter.\n";

echo date("H:i:s") . " - Using \"$masterServer\" as master server.\n";

$master = new MasterServer($masterServer);

echo date("H:i:s") . " - Trying to retrieve servers from \"$masterServer\"";
try {
    $servers = $master->getServers(MasterServer::REGION_ALL, $queryFilter, true);
} catch (Exception $e) {
    echo " Fail! ($e)\n";
}

echo " Success!\n";

$serverCount = 0;

foreach ($servers as $key => $server) {
    $serverCount++;
    echo date("H:i:s") . " - Master server replied with server[$serverCount]: $server[0]:$server[1]\n";
    $addresses[$serverCount] = "$server[0]:$server[1]";
}

$countReal = $serverCount++;

echo date("H:i:s") . " - Loaded $countReal servers from master server.\n";

if ($serverCount === 0) {
    die("No servers found!\nYou've probably only received 0 results because you've been raping the valve master server with too many requests. \nWait a little before trying again...");
}

$serverSuccessIp = [];
$statements = [];

$serverCount = 0;
$serverSuccessCount = 0;

foreach ($addresses as $address) {
    $addressSplit = explode(":", $address);
    $serverIp = $addressSplit[0];
    $serverPort = $addressSplit[1];
    $serverCount++;
    try {
        $record = $reader->country($serverIp);
        $serverRegion = ($record->country->isoCode);
    } catch (Exception $e) {
        echo date("H:i:s") . " - Unable to get region information for server $address. Setting country to \"North Korea\".\n";
        $serverRegion = "KP";
    }

    echo date("H:i:s") . " - Trying to get info for server[$serverCount]: $address";
    try {
        $query = new SourceQuery();
        $query->Connect($serverIp, $serverPort, 1, SQ_ENGINE);
        $serverInfo = $query->GetInfo();

        try {
            $serverRulesInfo = json_encode($query->GetRules());
        } catch (Exception $e) {
            $serverRulesInfo = "no server rules information";
        }

        try {
            $serverPlayerInfo = json_encode($query->GetPlayers());
        } catch (Exception $e) {
            $serverPlayerInfo = "no player information";
        }
        if (empty($serverInfo)) {
            $error = "Unable to retrieve server info";
            throw new Error($error);
        }

        if (in_array($address, $blacklist)) {
            $error = "Server was in blacklist";
            throw new Error($error);
        }

        $serverSuccessCount++;
        $serverSuccessIp[$serverSuccessCount] = $serverIp;

        $serverHostname = $serverInfo["HostName"];
        $serverModDesc = $serverInfo["ModDesc"];
        $serverPlayers = $serverInfo["Players"];
        $serverMaxPlayers = $serverInfo["MaxPlayers"];
        $serverMap = $serverInfo["Map"];

        if (strpos($serverHostname, "'") !== false) {
            $serverHostname = str_replace("'", "&apos;", $serverHostname);
        }

        $statements[$serverSuccessCount] = "INSERT INTO 'servers' (nr, ip, hostname, modDesc, players, maxPlayers, map, countryCode,playerInfo,serverRules)
            VALUES ($serverSuccessCount,'$address','$serverHostname','$serverModDesc','$serverPlayers','$serverMaxPlayers','$serverMap','$serverRegion','$serverPlayerInfo','$serverRulesInfo')";

        if ($serverPlayerInfo === "no player information") {
            echo " Success! (Unable to retrieve player info)\n";
        } else {
            echo " Success!\n";
        }
    } catch (Exception $e) {
        echo " Fail ($e)!\n";
    } catch (Error $e) {
        echo " Fail! ($error)\n";
    }
    finally {
        $query->Disconnect();
    }
}

echo date("H:i:s") . " - Trying to connect to database \"$database\"";
if (file_exists("../database/$database")) {
    $db = new SQLite3("../database/" . $database, SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);
    echo " Success!\n";
} else {
    die(" Fail! (Please run setupDatabase.php)\n");
}

echo date("H:i:s") . " - Trying to wipe previous servers and timestamp from the database.\n";
try {
    $db->exec("DELETE FROM 'servers'");
    $db->exec("DELETE FROM 'last-updated'");
} catch (Exception $e) {
    echo date("H:i:s") . " - Failed to delete previous servers from the database.\n";
}

echo date("H:i:s") . " - Trying to populate database with $serverSuccessCount new servers.\n";
try {
    foreach ($statements as $key => $statement) {
        try {
            echo date("H:i:s") . " - Inserting info from server[$key]: $serverSuccessIp[$key] to database.\n";
            $db->exec($statement);
        } catch (Exception $e) {
            echo date("H:i:s") . " - Failed to insert info from server[$key]: $serverSuccessIp[$key] into database!\n";
        }
    }
} catch (Exception $e) {
    echo date("H:i:s") . " - Failed to populate database.\n";
}

echo date("H:i:s") . " - Trying to insert timestamp into database.";
$timestamp = date("Y-m-d H:i:s");
try {
    $db->exec("INSERT INTO 'last-updated' (timestamp) VALUES ('$timestamp')");
    echo " Success!\n";
} catch (Exception $e) {
    echo " Fail!\n";
}

echo "Done. 👌\n";