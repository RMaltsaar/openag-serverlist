<?php

require_once("../config.php");

echo "Connecting to database \"" . $database . "\".\n";

if (file_exists("../database/$database")) {
    $db = new SQLite3("../database/" . $database, SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);
} else {
    die("  Database \"$database\" doesn't exist! (Run setupDatabase.php)");
}

echo "  Emptying all tables.\n";
$db->exec("DELETE FROM 'servers'");
$db->exec("DELETE FROM 'last-updated'");

echo "Done!";