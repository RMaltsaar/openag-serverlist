<?php

require_once("../config.php");

echo "Checking if database already exists...\n";
if (file_exists("../database/$database")) {
    die("  Database already exists!\n");
}

echo "  Database doesn't exist.\nCreating database \"" . $database . "\".\n";
if (!file_exists("../database")) {
	mkdir("../database");
}
$db = new SQLite3("../database/" . $database, SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);

echo "Creating tables: \n  nr\n  ip\n  hostname\n  modDesc\n  players\n  maxPlayers\n  map\n  countryCode\n  timestamp\n  playerInfo\n  serverRules\n";

$db->query('CREATE TABLE IF NOT EXISTS "servers" (
    "nr" INTEGER,
    "ip" VARCHAR,
    "hostname" VARCHAR,
    "modDesc" VARCHAR,
    "players" INTEGER,
    "maxPlayers" INTEGER,
    "map" VARCHAR,
    "countryCode" VARCHAR,
    "playerInfo" VARCHAR,
    "serverRules" VARCHAR
)');
$db->query('CREATE TABLE IF NOT EXISTS "last-updated" (
    "timestamp" VARCHAR
)');

echo "Done! ...You can now use the serverlist.";