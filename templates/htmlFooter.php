        </div>
		<section class="hero is-primary is-info is-bold">
			<div class="hero-body">
				<div class="container animated fadeInUp">
					<span>Made by bldsuu</span>
					<br>
					<span>Thanks to: High Tide, Execut4ble and [TC]Čiαиєz for help and feedback</span>
					<?= !empty($serversLastUpdated) ? '<span class="is-pulled-right">Last updated: ' . $serversLastUpdated[0]["timestamp"] . " EEST</span>" : '' ?>
				</div>
			</div>
		</section>
		<script src='/js/tablesort.js'></script>
		<script src='/js/sorts/tablesort.number.js'></script>
		<script>
			'use strict';

			document.addEventListener('DOMContentLoaded', function () {

			  // Modals

			  var rootEl = document.documentElement;
			  var $modals = getAll('.modal');
			  var $modalButtons = getAll('.modal-button');
			  var $modalCloses = getAll('.modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button');

			  if ($modalButtons.length > 0) {
			    $modalButtons.forEach(function ($el) {
			      $el.addEventListener('click', function () {
			        var target = $el.dataset.target;
			        var $target = document.getElementById(target);
			        rootEl.classList.add('is-clipped');
			        $target.classList.add('is-active');
			      });
			    });
			  }

			  if ($modalCloses.length > 0) {
			    $modalCloses.forEach(function ($el) {
			      $el.addEventListener('click', function () {
			        closeModals();
			      });
			    });
			  }

			  document.addEventListener('keydown', function (event) {
			    var e = event || window.event;
			    if (e.keyCode === 27) {
			      closeModals();
			    }
			  });

			  function closeModals() {
			    rootEl.classList.remove('is-clipped');
			    $modals.forEach(function ($el) {
			      $el.classList.remove('is-active');
			    });
			  }

			  // Functions

			  function getAll(selector) {
			    return Array.prototype.slice.call(document.querySelectorAll(selector), 0);
			  }

			});
		</script>
		<script>
			new Tablesort(document.getElementById('table'), {
				descending: true
			});
		</script>
	</body>
</html>