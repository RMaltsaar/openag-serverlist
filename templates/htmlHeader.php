<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8">
        <title>Adrenaline Gamer Dedicated Servers</title>
		<link rel="stylesheet" href="/css/bulma.min.css">
		<link rel="stylesheet" href="/css/style.css">
		<link rel="stylesheet" href="/css/animate.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans" rel="stylesheet"> 
        <link rel="icon" type="image/png" href="/images/fisk.png"/>
        <meta name="description" content="A list of Adrenaline Gamer Dedicated Servers">
		<meta name="keywords" content="adrenaline gamer,ag,openag,halflife,half life,half-life,hl,hldm">
		<meta name="author" content="bldsuu">
	</head>
	<body>
		<section class="hero is-primary is-info is-bold marginDown">
			<div class="hero-body">
				<div class="container animated fadeInDown">
					<a href="">
						<img src="/images/fisk.png" alt="fisk" class="logo">
						<h1 class="mainTitle">
							ADRENALINE GAMER DEDICATED SERVERS
						</h1>
					</a>
				</div>
			</div>
		</section>
		<div class="container animated fadeIn">