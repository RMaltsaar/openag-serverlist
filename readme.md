# openag-serverlist

A web application to display servers for goldsource or source games.

## Requirements

php7, composer

## Configuration

In your webserver config make sure only the public/ directory is visible on the web.

You can edit config.php to your own liking. By default it's set to retrieve ag servers.

## Installation

First you need to acquire the dependacies with composer.

	composer install
    
Next you need to create the database. 

	cd scripts
	php setupDatabase.php

Next you need to populate/update the database with server info.

	php updateDatabase.php

To make this automatic setup a systemd timer. (sample files in extras folder)

If you want to empty the database you can run the emptyDatabase.php script.

	php emptyDatabase.php

This Product includes GeoLite2 data created by MaxMind, available from <a href="http://www.maxmind.com">http://www.maxmind.com</a>.